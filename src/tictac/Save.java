package tictac;

import java.io.*;

public class Save {
    private static String string = "2";
    private static File meFile = new File("resources/Save.txt");
    private static char[] c = new char[10];
    private static int n = 0;
    private static int g = 0;

    static void save(boolean turnX) {
        if (turnX == true) {
            string = "T";
        } else {
            string = "N";
        }
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(meFile, true));/*BufferedWriter-Буферизированный выходной символьный поток
           FileWriter-Выходной поток, пишущий в файл */
            writer.write(string);
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    static void save(Tile tile) {
        if (tile.getValue().isEmpty()) {
            string = "2";
        }
        if (tile.getValue() == "X") {
            string = "X";
        }
        if (tile.getValue() == "O") {
            string = "O";
        }
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(meFile, true));/*BufferedWriter-Буферизированный выходной символьный поток
           FileWriter-Выходной поток, пишущий в файл */
            writer.write(string);
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    static void clean() {
        try {
            FileWriter fstream = new FileWriter(meFile);// конструктор с одним параметром - для перезаписи
            BufferedWriter writer = new BufferedWriter(fstream);
            writer.write("");
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    static char restore(int p) {
        if (g == 0) {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(meFile))) {
                int d = bufferedReader.read();
                while (d != -1) {  // Когда дойдём до конца файла, получим '-1'
                    c[n] = (char) d;
                    n++;
                    d = bufferedReader.read(); // Читаем символ
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        g++;
        return c[p - 1];
    }
}


