package tictac;

import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.rotateRight;

public class Server {
    static int port;
    static String IPt;
    static boolean read = true;
    public static InputStream sin;
    public static OutputStream sout;
    public static DataInputStream srin;
    public static DataOutputStream srout;

    public static void display(String tile) {
        Pane pane = new Pane();
        String cod = "-fx-background-color: #87CEFA";
        Label label = new Label("Введите данные");
        Button EnterBut = new Button("Ввел");
        Label IP = new Label("IP");
        Label portNumb = new Label("Port");
        TextField IpText = new TextField();
        TextField portText = new TextField();
        IpText.setTranslateY(63);
        IpText.setTranslateX(30);
        portText.setTranslateY(30);
        portText.setTranslateX(30);
        EnterBut.setStyle(cod);
        EnterBut.setTranslateY(100);
        EnterBut.setTranslateX(0);
        portNumb.setTranslateY(33);
        IP.setTranslateY(66);
        Stage ServWindow = new Stage();
        ServWindow.initModality(Modality.APPLICATION_MODAL);
        ServWindow.setTitle(tile);
        ServWindow.setHeight(170);
        ServWindow.setWidth(320);
        ServWindow.setResizable(false);
        ServWindow.setTitle(tile);

        EnterBut.setOnAction(ActionEvent -> {
            try {
                read=true;
                port = parseInt(portText.getText());
            } catch (NumberFormatException nfe) {
                ServWindow.close();
                Server.display("Создание");
                ErrorData.display("Ошибка");
                read = false;
            }
            IPt = IpText.getText();
            if (read == true) {
                if (1025 < port && port < 65535) {
                    try {
                        ServerSocket ss = new ServerSocket(port);
                        Wait.display("Ждем");
                        ServWindow.close();
                        Socket socket = ss.accept();
                        Wait.closses();
                       sin = socket.getInputStream();
                        sout = socket.getOutputStream();
                       srin = new DataInputStream(sin);
                       srout = new DataOutputStream(sout);
                        ServerWork thred=new ServerWork();
                        thred.start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    ServWindow.close();
                    Server.display("Создание");
                    ErrorData.display("Ошибка");
                }
            }
        });
        pane.getChildren().addAll(label, EnterBut, IP, portNumb, IpText, portText);
        Scene scene = new Scene(pane);
        ServWindow.setScene(scene);
        ServWindow.show();
    }


}
