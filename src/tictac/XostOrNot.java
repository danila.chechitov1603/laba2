package tictac;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;



public class XostOrNot {
   static boolean clientGame=true;
    public static void display(String tile) {
        GameLogick gameLogick=new GameLogick();
        Pane pane = new Pane();
        String cod = "-fx-background-color: #87CEFA";
        Label label=new Label(" Вы хотите быть хостом или подключится к созданной игре?");
        Button xostBut=new Button("Создать");
        Button conectBut=new Button("Подключится");
        xostBut.setStyle(cod);
        conectBut.setStyle(cod);
        xostBut.setTranslateY(25);
        xostBut.setTranslateX(20);
        conectBut.setTranslateY(25);
        conectBut.setTranslateX(200);
        Stage ChosseWindow = new Stage();
        ChosseWindow.initModality(Modality.APPLICATION_MODAL);
        ChosseWindow.setTitle(tile);
        ChosseWindow.setHeight(100);
        ChosseWindow.setWidth(320);
        ChosseWindow.setResizable(false);
        ChosseWindow.setTitle(tile);
        xostBut.setOnAction(event -> {
            ChosseWindow.close();
            clientGame=false;
            gameLogick.turnX=true;
            Server.display("Создание");
        });
        conectBut.setOnAction(event -> {
            ChosseWindow.close();
            clientGame=true;
            gameLogick.turnX=true;
            Client.display("Подключение");
        });
        pane.getChildren().addAll(label,conectBut,xostBut);
        Scene scene = new Scene(pane);
        ChosseWindow.setScene(scene);
        ChosseWindow.show();
    }
}

