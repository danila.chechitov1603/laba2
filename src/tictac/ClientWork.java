package tictac;

import javafx.application.Platform;

import java.io.IOException;

import static java.lang.Integer.parseInt;
import static tictac.Client.clin;
import static tictac.GameLogick.tileList;
import static tictac.GameLogick.turnX;

public class ClientWork extends Thread {
    public void run() {
        while (true) {
            try {
                if(!GameLogick.noWinners){
                    sleep(3000);
                    wait();
                }
                if (!clin.readUTF().isEmpty()) {
                   GameLogick gameLogick = new GameLogick();
                    String drownumber1 = clin.readUTF();
                    int drownumber = parseInt(drownumber1);
                    for (int i = 0; i < 9; i++) {
                        String temp = tileList.get(i).number;
                        int tempnumber = parseInt(temp);
                        if (tempnumber == drownumber) {
                            tileList.get(i).drowX();
                           gameLogick.check();
                          turnX=true;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
