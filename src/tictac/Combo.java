package tictac;

public class Combo {
    protected Tile[] tiles;
    GameLogick gameLogick=new GameLogick();
    /**
     * в тайле хранится 3 элемента нашего комбо позиция первого поля втрой и третей которое проверяем
     */

    public Combo(Tile... tiles) {//Для указания аргумента переменной длины используют три точки (...) аргументов может быть и 0.
        this.tiles = tiles;
        /*
         * тут мы передаем tiles в масив tiles
         *  в комбо в качестве арг передаются  наши комбинации (board[0][0], board[1][1], board[2][2]) и в итоге
         *  у нас есть комбо в которых хранится масив tiles в качестве жлементов которого выступают тайлы для комбо
         *в следствие чего  у нас есть 8 разных типов комбинаций в которых хранится масив tiles в нутри которых хранятся точки этих комбинаций*/
    }

    /**
     * в этой функции мы проверяем выполнил ли кто-то из игроков или бот комбо сначала мы проверяем является ли 1 тайл комбо пустым если да то выходим ибо
     * это автоматически азначает что комбо не выполнен а вот если он не пусть то мы сравниваем текст из 1 тайла с текстов в 2 и 3 тайлах и если он
     * совпал то мы передаем значение true что значит что игрок или бот выйграл
     */
    public boolean complete() {
        if (tiles[0].getValue().isEmpty()) {
            return false;
        }
        return tiles[0].getValue().equals(tiles[1].getValue())
                && tiles[0].getValue().equals(tiles[2].getValue());//смотрим одинаковые ли значения в тайлах
    }


    /**
     * данный алгорит очень прост мы сначала сморим пустойли ли n тайл из нашей комбинации если нет то мы сравниваем его со значением
     * которое хранится в другом тайле комбинации если оно совпало то мы проверяем пустой оставшийся тайл из комбинации(тот к которому мы
     * еще не обращались в разных проверках 1 ,2 и 3 если он пусть то мы смотрим какой символ пишет игрок если X то мы записываем
     * в пустой тайл  O и наоборот основной смысл данного алгоритма заблокировать комбинацию противника и закончить своюю)
     * после чего вызываем функцию check которая смотрит есть ли побидитель или закончилась ли игра
     */
    public void noWinCombo() {
        if (!tiles[0].getValue().isEmpty() && tiles[0].getValue().equals(tiles[1].getValue()) && tiles[2].getValue().isEmpty()) {
            if (GameLogick.turnX) {
                tiles[2].drow0();
                GameLogick.botTurn = false;
                gameLogick.check();
            } else {
                tiles[2].drowX();
                GameLogick.botTurn = false;
                gameLogick.check();
            }
        } else if (!tiles[0].getValue().isEmpty() && tiles[0].getValue().equals(tiles[2].getValue()) && tiles[1].getValue().isEmpty()) {
            if (GameLogick.turnX) {
                tiles[1].drow0();
                GameLogick.botTurn = false;
                gameLogick.check();
            } else {
                tiles[1].drowX();
                GameLogick.botTurn = false;
                gameLogick.check();
            }
        } else if (!tiles[1].getValue().isEmpty() && tiles[1].getValue().equals(tiles[2].getValue()) && tiles[0].getValue().isEmpty()) {
            if (GameLogick.turnX) {
                tiles[0].drow0();
                GameLogick.botTurn = false;
                gameLogick.check();
            } else {
                tiles[0].drowX();
                GameLogick.botTurn = false;
                gameLogick.check();
            }
        }
    }

    /**
     * данный алгорит выполняется после алгоритма noWinCombo и он нужен нам если нечего блокировать
     * в этом алгоритме мы просто смотрим пусть ли n-ый тайл комбинаии если да то мы смотрим какой символ пишет игрок если X то мы записываем
     * в пустой тайл  O и наоборот основной смысл данного алгоритма заблокировать комбинацию противника и закончить своюю)
     * после чего вызываем функцию check которая смотрит есть ли побидитель или закончилась ли игра
     */
    public void tryToWin() {
        if (tiles[0].getValue().isEmpty()) {
            if (GameLogick.turnX) {
                tiles[0].drow0();
                GameLogick.botTurn = false;
                gameLogick.check();
            } else {
                tiles[0].drowX();
                GameLogick.botTurn = false;
                gameLogick.check();
            }
        } else if (tiles[1].getValue().isEmpty()) {
            if (GameLogick.turnX) {
                tiles[1].drow0();
                GameLogick.botTurn = false;
                gameLogick.check();
            } else {
                tiles[1].drowX();
                GameLogick.botTurn = false;
                gameLogick.check();
            }
        } else if (tiles[2].getValue().isEmpty()) {
            if (GameLogick.turnX) {
                tiles[2].drow0();
                GameLogick.botTurn = false;
                gameLogick.check();
            } else {
                tiles[2].drowX();
                GameLogick.botTurn = false;
                gameLogick.check();
            }
        }
    }

}

