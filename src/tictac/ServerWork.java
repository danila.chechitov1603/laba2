package tictac;

import javafx.application.Platform;

import java.io.IOException;

import static java.lang.Integer.parseInt;
import static tictac.GameLogick.tileList;
import static tictac.GameLogick.turnX;
import static tictac.Server.srin;

public class ServerWork extends Thread {
    public void run() {
        while (true) {
            try {
                if (!GameLogick.noWinners) {
                    sleep(3000);
                    wait();
                }
                if (!srin.readUTF().isEmpty()) {
                    GameLogick gameLogick = new GameLogick();
                    String drownumber1 = srin.readUTF();
                    int drownumber = parseInt(drownumber1);
                    for (int i = 0; i < 9; i++) {
                        String temp = tileList.get(i).number;
                        int tempnumber = parseInt(temp);
                        if (tempnumber == drownumber) {
                            tileList.get(i).drow0();
                            gameLogick.check();
                            turnX = true;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
