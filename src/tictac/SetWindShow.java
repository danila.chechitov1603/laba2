package tictac;


import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;


public class SetWindShow {

    private static String Iks ="Крестик";
    private static String nul="Нолик";
    private static String Gamer="Игрока";
    private static String Bot="Бота";
    private static String No="Нет";
    private static String Yes="Да";
    public static Label label = new Label(Iks);
    public static Label label1 = new Label(Gamer);
    public static Label multLabel = new Label(No);
    public static void display(String tile) {

        Pane pane = new Pane();
        Stage setingWindow = new Stage();
        ImageView iv = new ImageView();
        File file = new File("resources/Im2.jpg");
        Image image = new Image(file.toURI().toString());
        setingWindow.initModality(Modality.APPLICATION_MODAL);
        setingWindow.setTitle(tile);
        setingWindow.setHeight(250);
        setingWindow.setWidth(250);
        setingWindow.setResizable(false);
        setingWindow.setTitle(tile);
         String cod = "-fx-background-color: #87CEFA";
        Button closse = new Button("Назад");
        Button gameType = new Button("Играть против");
        Button whoFirst = new Button("Первым ходит");
        Button multiplayer = new Button("Играть по сети?");
        closse.setStyle(cod);
        gameType.setStyle(cod);
        whoFirst.setStyle(cod);
        multiplayer.setStyle(cod);
        closse.setTranslateY(120);
        gameType.setTranslateY(40);
        multiplayer.setTranslateY(80);
        label.setTranslateX(130);
        label.setTranslateY(5);
        label.setFont(Font.font(15));
        label.setTextFill(Color.WHITE);
        label1.setTranslateX(130);
        label1.setTranslateY(45);
        label1.setFont(Font.font(15));
        label1.setTextFill(Color.WHITE);
        multLabel.setTranslateX(130);
        multLabel.setTranslateY(85);
        multLabel.setFont(Font.font(15));
        multLabel.setTextFill(Color.WHITE);
        gameType.setPrefSize(120, 30);
        closse.setPrefSize(120, 30);
        whoFirst.setPrefSize(120, 30);
        multiplayer.setPrefSize(120, 30);
        closse.setOnAction(event -> {
            setingWindow.close();
        });
        whoFirst.setOnAction(event -> {/**
         *если нажали на кнопку то текст записаный в Label на основе которого решается кто ходит первым
         */
            if (label.getText() == nul) {
                label.setText(Iks);
            } else label.setText(nul);
        });
        gameType.setOnAction(event -> {
            /**
             *если нажали на кнопку то текст записаный в Label на основе которого решается тип игры
             */
            if (label1.getText() == Gamer) {
                label1.setText(Bot);
            } else label1.setText(Gamer);
        });
        multiplayer.setOnAction(event -> {/**
         *если нажали на кнопку то текст записаный в Label на основе которого решается кто ходит первым
         */
            if (multLabel.getText() == Yes) {
                multLabel.setText(No);
            } else multLabel.setText(Yes);
        });
        iv.setImage(image);
        pane.getChildren().addAll(closse, whoFirst, iv, label, label1, gameType,multiplayer,multLabel);
        iv.toBack();
        Scene scene = new Scene(pane);
        setingWindow.setScene(scene);
        setingWindow.show();
    }

}

