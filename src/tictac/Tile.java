package tictac;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.IOException;

import static java.lang.Integer.parseInt;
import static tictac.Client.clin;
import static tictac.Client.clout;
import static tictac.GameLogick.*;
import static tictac.Server.srin;
import static tictac.Server.srout;
import static tictac.XostOrNot.clientGame;


public class Tile extends StackPane {
    /**
     * StackPane это контейнер содержащий разные компоненты интерфейса которые расставленны в стопку(есть те что на переднем
     * плане и на заднем мы видем только те что в самом верху) поставить компонент вперед команда toFront а в низ toBack так же при помощи команды
     * Node.setVisible(visible) модно делать невидимые линии видимыми
     */
    int n;
    public String number = new String();
    private Text text = new Text(null);//теперь при создании тайла он по умаолчанию будет иметь текст


    public Tile() {
        Rectangle border = new Rectangle(200, 200);/**
         *Rectangle это класс помогающий вам нарисовать прямоугольник
         * дальше мы устанавливаем цвет внутри прямоугольнака размер букв в нем и цвет линий*/
        GameLogick gameLogick = new GameLogick();
        border.setFill(null);//цвет внутри прямоугольника
        border.setStroke(Color.BLACK);//цвет линий нашего прямогульника
        text.setFont(Font.font(76));//размер букв
        getChildren().addAll(border, text);
        setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {/**
             *мы можем использовать этот метод потому что мы extends(вытягиваем??) его из StackPane который в свою очередь
             *   выходит из Node который содержит данный метод(зажав контрол можно пройтись по этому)*/
                if (multGame) {
                    if (clientGame == true) {
                        if (!noWinners) {
                            gameLogick.check();
                            return;
                        }
                        if (turnX == true) {
                            try {
                                if (text.getText() != "X" && text.getText() != "O") {
                                    clout.writeUTF(number);
                                    if (n == 1) {
                                        multiWork(event);
                                        turnX = false;
                                        n = 0;
                                    }
                                    n++;
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        if (!noWinners) {
                            gameLogick.check();
                            return;
                        }
                        if (turnX == true) {
                            try {
                                if (text.getText() != "X" && text.getText() != "O") {
                                    srout.writeUTF(number);
                                    if (n == 1) {
                                        multiWork(event);
                                        turnX = false;
                                        n = 0;
                                    }
                                    n++;
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else

                    Work(event);
            }

        });
    }

    /**
     * данная функция нужна нам для получения центра тайла(чтобы линия в animation шла красиво поцентру)
     */
    public double getCenterX() {
        return getTranslateX() + 100;
    }

    /**
     * данная функция нужна нам для получения центра тайла(чтобы линия в animation шла красиво поцентру)
     */
    public double getCenterY() {
        return getTranslateY() + 100;
    }

    /**
     * данная функция нужна нам для точго чтобы написать Х в тайл
     */
    public void drowX() {
        text.setText("X");
    }

    /**
     * данная функция нужна нам для точго чтобы написать 0 в тайл
     */
    public void drow0() {
        text.setText("O");
    }

    /**
     * данная функция нужна нам для точго чтобы получить текст записанный в тайл
     */
    public String getValue() {
        return text.getText();
    }

    public void Work(MouseEvent event) {
        GameLogick gameLogick = new GameLogick();
        if (!GameLogick.botGame) {/**проверяем это игра с ботом или 2 игроков*/
            if (!GameLogick.noWinners) {/**если уже есть побидитель или закончилась игра то мы вызываем проверку*/
                gameLogick.check();
                return;
            }
            if (event.getButton() == MouseButton.PRIMARY) {//(PRIMARY это левая)
                if (GameLogick.turnX && text.getText() != "O" && text.getText() != "X") {/**смотрим есть ли в тайле уже текcт
                 +если нет то пишем в него X делаем проверку на комбо и передаем ход другому игроку
                 */
                    drowX();
                    GameLogick.turnX = false;
                    gameLogick.check();
                } else
                    return;
            } else if (event.getButton() == MouseButton.SECONDARY) {//правая
                if (!GameLogick.turnX && text.getText() != "X" && text.getText() != "O") {
                    /**
                     * тоже самое что делали до этого только теперь записываем 0*/
                    drow0();
                    GameLogick.turnX = true;
                    gameLogick.check();
                } else
                    return;
            }
        } else {
            if (!GameLogick.noWinners) {
                gameLogick.check();
                return;
            }
            if (event.getButton() == MouseButton.PRIMARY) {//(PRIMARY это левая)
                if (GameLogick.turnX) {
                    if (text.getText() != "O" && text.getText() != "X") {/**смотрим есть ли в тайле уже текcт
                     *если нет то пишем в него x делаем проверку на комбо и передаем ход боту*/
                        drowX();
                        GameLogick.botTurn = true;
                        gameLogick.check();
                        GameLogick.botGo();
                    }
                } else {
                    if (text.getText() != "O" && text.getText() != "X") {
                        /**
                         * тоже самое что делали до этого только теперь записываем 0*/
                        drow0();
                        GameLogick.botTurn = true;
                        gameLogick.check();
                        GameLogick.botGo();
                    }
                }
            }
        }
    }

    public void multiWork(MouseEvent event) {
        GameLogick gameLogick = new GameLogick();
        if (!GameLogick.noWinners) {/**если уже есть побидитель или закончилась игра то мы вызываем проверку*/
            gameLogick.check();
            return;
        }
        if (event.getButton() == MouseButton.PRIMARY) {//(PRIMARY это левая)
            if (GameLogick.turnX && text.getText() != "O" && text.getText() != "X") {/**смотрим есть ли в тайле уже текcт
             +если нет то пишем в него X делаем проверку на комбо и передаем ход другому игроку
             */
                if (clientGame == true) {
                    drow0();
                } else {
                    drowX();
                }
                gameLogick.check();
            } else
                return;
        }
    }
}


