package tictac;


import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import static java.lang.Integer.parseInt;

public class Client {
    static int port;
    static String IPt;
    static boolean read = true;
    public static InputStream cin;
    public static OutputStream cout;
    public static DataInputStream clin;
    public static DataOutputStream clout;

    public static void display(String tile) {
        Pane pane = new Pane();
        String cod = "-fx-background-color: #87CEFA";
        Label label = new Label("Введите данные");
        Button EnterBut = new Button("Ввел");
        Label IP = new Label("IP");
        Label portNumb = new Label("Port");
        TextField IpText = new TextField();
        TextField portText = new TextField();
        IpText.setTranslateY(63);
        IpText.setTranslateX(30);
        portText.setTranslateY(30);
        portText.setTranslateX(30);
        EnterBut.setStyle(cod);
        EnterBut.setTranslateY(100);
        EnterBut.setTranslateX(0);
        portNumb.setTranslateY(33);
        IP.setTranslateY(66);
        Stage clientWindow = new Stage();
        clientWindow.initModality(Modality.APPLICATION_MODAL);
        clientWindow.setTitle(tile);
        clientWindow.setHeight(170);
        clientWindow.setWidth(320);
        clientWindow.setResizable(false);
        clientWindow.setTitle(tile);
        EnterBut.setOnAction(ActionEvent -> {
            try {
                read = true;
                port = parseInt(portText.getText());
            } catch (NumberFormatException nfe) {
                clientWindow.close();
                Client.display("Подключение");
                ErrorData.display("Ошибка");
                read = false;
            }
            IPt = IpText.getText();
            if (read == true) {
                if (1025 < port && port < 65535) {
                    try {
                        InetAddress ipAddress = InetAddress.getByName(IPt);
                        Socket socket = new Socket(ipAddress, port);
                        GameLogick.turnX = false;
                        cin = socket.getInputStream();
                        cout = socket.getOutputStream();
                        clin = new DataInputStream(cin);
                        clout = new DataOutputStream(cout);
                        ClientWork thred=new ClientWork();
                        thred.start();

                    } catch (IOException e) {
                        clientWindow.close();
                        Client.display("Подключение");
                        ErrorData.display("Ошибка");
                        read = false;
                    }
                    clientWindow.close();
                } else {
                    clientWindow.close();
                    Client.display("Подключение");
                    ErrorData.display("Ошибка");
                }
            }
        });
        pane.getChildren().addAll(label, EnterBut, IP, portNumb, IpText, portText);
        Scene scene = new Scene(pane);
        clientWindow.setScene(scene);
        clientWindow.show();
    }
}

