package tictac;


import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;


import java.io.File;

public class ErrorData {
    public static void display(String tile) {
        Pane pane = new Pane();
        String cod = "-fx-background-color: #87CEFA";
        Label label=new Label("Введенны неверные данные проверьте и введите другие");
        Button Ok=new Button("Ок");
        Ok.setStyle(cod);
        Ok.setTranslateY(25);
        Ok.setTranslateX(120);
        Stage ErrorWindow = new Stage();
        ErrorWindow.initModality(Modality.APPLICATION_MODAL);
        ErrorWindow.setTitle(tile);
        ErrorWindow.setHeight(100);
        ErrorWindow.setWidth(320);
        ErrorWindow.setResizable(false);
        ErrorWindow.setTitle(tile);
        Ok.setOnAction(event -> {
            ErrorWindow.close();
    });

        pane.getChildren().addAll(label,Ok);
        Scene scene = new Scene(pane);
        ErrorWindow.setScene(scene);
        ErrorWindow.show();
    }
}
