package tictac;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Wait {
   static Stage waitWindow = new Stage();
    public static void display(String tile) {
        Pane pane = new Pane();
        waitWindow.initModality(Modality.APPLICATION_MODAL);
        waitWindow.setTitle(tile);
        waitWindow.setHeight(100);
        waitWindow.setWidth(320);
        waitWindow.setResizable(false);
        waitWindow.setTitle(tile);
        Scene scene = new Scene(pane);
        waitWindow.setScene(scene);
        waitWindow.show();
    }
  static public void closses(){
        waitWindow.close();
  }
}
