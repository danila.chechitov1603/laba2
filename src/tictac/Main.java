package tictac;

import javafx.application.Application;
import javafx.stage.Stage;



public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {//запуск
        GameLogick gameLogick=new GameLogick();
        gameLogick.stage = primaryStage;//делаем так для того чтобы можно было закрыть нынешнее окно при создании нового
        gameLogick.startNewGame(primaryStage);
    }



    public static void main(String[] args) {
        launch(args);
    }
}
