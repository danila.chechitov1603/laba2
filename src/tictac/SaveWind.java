package tictac;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;


import java.io.File;

public class SaveWind {
   static public Label answer=new Label();
    public static void display(String tile) {
        Pane pane = new Pane();
        String cod = "-fx-background-color: #87CEFA";
        Label label=new Label(" Найденно сохранение хотите востановить прогресс?");
        Button yesBut=new Button("Да");
        Button noBut=new Button("Нет");
        yesBut.setStyle(cod);
        noBut.setStyle(cod);
        yesBut.setTranslateY(25);
        yesBut.setTranslateX(120);
        noBut.setTranslateY(25);
        noBut.setTranslateX(160);
        Stage saveWindow = new Stage();
        saveWindow.initModality(Modality.APPLICATION_MODAL);
        saveWindow.setTitle(tile);
        saveWindow.setHeight(100);
        saveWindow.setWidth(320);
        saveWindow.setResizable(false);
        saveWindow.setTitle(tile);
        yesBut.setOnAction(event -> {
           answer.setText("Да");
           saveWindow.close();
           GameLogick.restoration();
        });
        noBut.setOnAction(event -> {
            answer.setText("Нет");
            saveWindow.close();
        });
        pane.getChildren().addAll(label,noBut,yesBut);
        Scene scene = new Scene(pane);
        saveWindow.setScene(scene);
        saveWindow.show();
    }
}
