package tictac;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import static tictac.XostOrNot.clientGame;

public class GameLogick {
    public static boolean noWinners = true;
    public static boolean botTurn = false;
    public static boolean botGame = false;
    public static boolean multGame = false;
    public static boolean turnX = true;
    private static int j = 0;
    int number;
    static public Stage stage;
    private static Pane root = new Pane();
    public static Scene scene2 = new Scene(root, 600, 600);
    private static int n = 0;//считает количество вписанных букв если 9 выходим
    private static StackPane stackPane = new StackPane();//нужна потому что без нее не созд заново
    private static Scene scene = new Scene(stackPane, 600, 600);
    public static List<Tile> tileList = new ArrayList<>();
    protected static List<Combo> combo = new ArrayList<>();
    private String cod = "-fx-background-color: #87CEFA";
    private Tile[][] board = new Tile[3][3];
    private Button startGame = new Button("Начать играть");
    private Button endGame = new Button("Выход");
    private Button settings = new Button("Настройки");
    private ImageView iv = new ImageView();
    private File file = new File("resources/Im.jpg");
    private Image image = new Image(file.toURI().toString());

    public void startNewGame(Stage stage2) {
/**
 * задаем рамеры нашего экрана ставим на задний фон картинку и прочие мелочи*/
        startGame.setPrefSize(120, 40);
        startGame.setStyle(cod);
        endGame.setStyle(cod);
        settings.setStyle(cod);
        endGame.setPrefSize(120, 40);
        settings.setPrefSize(120, 40);
        stage2.setResizable(false);//чтобы не растягивать окно
        stage2.setTitle("Крестики нолики");
        endGame.setTranslateY(100);
        settings.setTranslateY(50);
        iv.setImage(image);
        /**
         * если у нас уже есть победитель и это перезапуск игры то мы просто устанавливаем scene а если
         * это первый запуск игры то мы передаем в stackPane(на основе кот создается scene) все элементы которые мы создали
         * после чего ставим картинку на задний фон(не добавляем а говорим ей что она имеет наименьший приаритет и все кнопки будут поверх ее)*/
        if (!noWinners) {
            stage2.setScene(scene);
        } else {
            stackPane.getChildren().addAll(startGame, endGame, iv, settings);
            iv.toBack();
            stage2.setScene(scene);
        }
        /**обработчик нажатий на кнопки
         * 1)если нажали на endGame то мы выходим из игры
         *2) если на startGame то мы основываясь на значениях из
         *SetWindShow который является окном в котором происходит настройка игры(выбор типа игры(против бота или игрока) и кто ходит первый)
         * начинаем игру
         * 3) если на settings то открывает окно настроек где мы можим выбирать кто ходит первый и против кого играть */
        endGame.setOnAction(actionEvent -> {
            stage2.close();
        });

        startGame.setOnAction(actionEvent -> {//изменение правил игры из
            if (SetWindShow.label.getText() == "Крестик") {
                turnX = true;
            } else turnX = false;
            if (SetWindShow.label1.getText() == "Игрока") {
                botGame = false;
            } else botGame = true;
            if (SetWindShow.multLabel.getText() == "Нет") {
                multGame = false;
            } else {
                multGame = true;
                botGame = false;
            }
            stage2.setScene(scene2);
            try (BufferedReader br = new BufferedReader(new FileReader("resources/Save.txt"))) {
                if (br.readLine() != null && !multGame) {
                    SaveWind.display("Сохранение");
                }
                if (multGame == true) {
                    XostOrNot.display("Выбор");
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        settings.setOnAction(actionEvent -> {
            SetWindShow.display("Настройки");
        });
/**
 * если игра началась заново то очишаем все*/
        if (!noWinners) {
            root.getChildren().clear();
            combo.clear();
            n = 0;
            noWinners = true;
            Save.clean();
            tileList.clear();
        }
        /**
         * тут создаем тайлы и расставляем их ввиде матрицы*/
        for (int i = 0; i < 3; i++) {//создание тайлов для игры в виде матрицы
            for (int j = 0; j < 3; j++) {
                Tile tile = new Tile();
                number++;
                tile.setTranslateX(j * 200);
                tile.setTranslateY(i * 200);
                tile.number = "" + number;
                root.getChildren().add(tile);
                board[j][i] = tile;//добавление тайлов в board для того чтобы мы ими пользовались в комбо
                tileList.add(board[j][i]);
            }
        }


/**
 * добавляем различные комбо в массив комбинаций*/
        for (int y = 0; y < 3; y++) {
            combo.add(new Combo(board[0][y], board[1][y], board[2][y]));//вводим виды комб тут горизонтальная
        }
        for (int x = 0; x < 3; x++) {
            combo.add(new Combo(board[x][0], board[x][1], board[x][2]));//вводим виды комб тут вертикальная
        }
        combo.add(new Combo(board[0][0], board[1][1], board[2][2]));////вводим виды комб тут диагональная \
        combo.add(new Combo(board[2][0], board[1][1], board[0][2]));////вводим виды комб тут диагональная /
        if (botTurn) {
            botTurn = false;
        }
        stage2.show();
    }

    public static void restoration() {
        int p = 1;
        if (SaveWind.answer.getText() == "Да") {
            if (p == 1) {
                if (Save.restore(p) == 'T') {
                    turnX = true;
                }
                if (Save.restore(p) == 'N') {
                    turnX = false;
                }
                p++;
            }
            if (p != 1) {
                for (Tile tile : tileList) {
                    if (Save.restore(p) == 'X') {

                        tile.drowX();
                    }
                    if (Save.restore(p) == 'O') {
                        tile.drow0();
                    }
                    p++;
                }
            }
        }
    }

    public static void botGo() {
        for (Combo bot : combo) {//сначала бот пытается заблокировать комбо противника
            if (botGame) {
                if (botTurn) {
                    bot.noWinCombo();
                }
            }
        }
        for (Combo bot : combo) {//если у противника нет 2 в ряд то он ходит в случ точку
            if (botGame) {
                if (botTurn) {
                    bot.tryToWin();
                }
            }
        }
    }

    void savick() {
        Save.clean();
        Save.save(turnX);
        for (Tile tile : tileList) {
            Save.save(tile);
        }
    }

    public void check() {//проверка на то есть ли комбо и не закончилось ли поле
        n++;
        savick();
        /**
         *считаем количесво совершенных ходов если 9 выходим потому что у нас доска 3*3 */
        if (n >= 9) {
            noWinners = false;
            botTurn = false;
            j++;
            if (j == 2) {//ждем клика мыши (задержка)
                startNewGame(stage);
                j = 0;
                n = 0;
            }
        }
        /**
         * проверка того есть ли законченная комбинация если да то вызывается функция animation */
        for (Combo combo : combo) {
            if (combo.complete()) {//проверка на то есть ли комбо
                noWinners = false;
                botTurn = false;
                    animation(combo);
                break;
            }
        }

    }

    protected void animation(Combo combo) {//анимация победы
        Line line = new Line();
        line.setStartX(combo.tiles[0].getCenterX());//начало
        line.setStartY(combo.tiles[0].getCenterY());
        line.setEndX(combo.tiles[0].getCenterX());
        line.setEndY(combo.tiles[0].getCenterY());//конец линии
        if(multGame) {
            Platform.runLater(() -> {
                root.getChildren().addAll(line);
            });
        }
        else {
            root.getChildren().addAll(line);
        }
        Timeline timeline = new Timeline();//Timeline класс отвечающий за анимацию
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),//задержка в рисовании Duration
                //KeyFrame-Определяет целевые значения в указанном моменте времени для ряда переменных, которые интерполируются вдоль a Timeline.
                new KeyValue(line.endXProperty(), combo.tiles[2].getCenterX()),//определяет начало конец фрейма первое это  цель а второе конечное значение
                new KeyValue(line.endYProperty(), combo.tiles[2].getCenterY())));
        timeline.play();//проигрывается анимация
        j++;
        if (j == 2) {//ждем нажатия мышкил
            startNewGame(stage);
            j = 0;
        }

    }

}
